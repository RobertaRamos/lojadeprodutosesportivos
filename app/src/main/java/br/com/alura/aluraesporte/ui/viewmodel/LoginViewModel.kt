package br.com.alura.aluraesporte.ui.viewmodel

import androidx.lifecycle.ViewModel
import br.com.alura.aluraesporte.repository.LoginRepository

class LoginViewModel(private val repositpry: LoginRepository): ViewModel() {

    fun loga(){
        repositpry.loga()
    }

    fun estaLogado(): Boolean{
        return repositpry.estaLogado()
    }

    fun desloga() {
        repositpry.desloga()
    }

    fun naoEstaLogado(): Boolean {
        return !estaLogado()
    }

}
